// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

UENUM(BlueprintType)
enum class ETipesOfFood : uint8
{
	STANDART = 0	UMETA(DisplayName = "STANDART"),
	UPSPEED = 1		UMETA(DisplayName = "UPSPEED"),
	DOWNSPEED = 2	UMETA(DisplayName = "DOWNSPEED"),
	DOUBLEMASS = 3
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWhenEated);

UCLASS()
class SNAKE_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ETipesOfFood TypeOfFood;

	UPROPERTY(VisibleAnywhere, BlueprintAssignable, Category = "EventDispatchers")
	FWhenEated WhenEated;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION(BlueprintCallable)
	void SetTypeOfFood();
};
