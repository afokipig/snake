// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Components/SceneComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMovementDirection = EMovementDirection::DOWN;
	MovementSpeed = 10;
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(6);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MovementSpeed);
	Move();
	LastTickMovementDirection = LastMovementDirection;
	

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		NewSnakeElem->MeshComponent->SetVisibility(false);
		NewSnakeElem->SnakeOwner = this;
		int32 SnakeElementIndex = SnakeElements.Add(NewSnakeElem);
		if (SnakeElementIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
		
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(0, 0, 0);
	float MovementDirection = ElementSize;
	
	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementDirection;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementDirection;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementDirection;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementDirection;
		break;
	}

	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		ASnakeElementBase* CurrentSnakeElem = SnakeElements[i];
		ASnakeElementBase* PrewSnakeElem = SnakeElements[i - 1];
		FVector SnakeElemLocation = PrewSnakeElem->GetActorLocation();
		CurrentSnakeElem->SetActorLocation(SnakeElemLocation);
		CurrentSnakeElem->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->MeshComponent->SetVisibility(true);
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SnakeDeath()
{
	for (int i = 0; i<SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}
	
}
