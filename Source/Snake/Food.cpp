// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"
#include "Containers/Array.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	SetTypeOfFood();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			switch (TypeOfFood)
			{
			case ETipesOfFood::STANDART:
			{
				Snake->AddSnakeElement();
				break;
			}
			case ETipesOfFood::UPSPEED:
			{
				Snake->MovementSpeed *= 0.9;
				break;
			}
			case ETipesOfFood::DOWNSPEED:
			{
				Snake->MovementSpeed *= 1.1;
				break;
			}
			case ETipesOfFood::DOUBLEMASS:
			{
				Snake->AddSnakeElement(2);
				break;
			}
			}
			WhenEated.Broadcast();
			Destroy();
		}
	}
	else
	{
		WhenEated.Broadcast();
		Destroy();
	}
}

void AFood::SetTypeOfFood()
{
	TArray<int32> Weghts;
	int32 StandartWeight = 100;
	Weghts.Add(StandartWeight);
	int32 UpSpeedWeight = 10;
	Weghts.Add(UpSpeedWeight);
	int32 DownSpeedWeight = 10;
	Weghts.Add(DownSpeedWeight);
	int32 DoubleMassWeight = 2;
	Weghts.Add(DoubleMassWeight);
	int32 SumOfWeghts = 0;

	for (int i = 0; i < Weghts.Num(); i++)
	{
		SumOfWeghts += Weghts[i];
	}
	int32 RandomNumber = FMath::RandRange(1, SumOfWeghts);
	for (int i = 0; i < Weghts.Num(); i++)
	{
		RandomNumber -= Weghts[i];
		if (RandomNumber <= 0)
		{
			TypeOfFood = StaticCast<ETipesOfFood>(i);
			break;
		}
	}

}

