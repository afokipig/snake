// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnakeGameModeBase.h"
#include "BPSnakeGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API ABPSnakeGameMode : public ASnakeGameModeBase
{
	GENERATED_BODY()
	
};
